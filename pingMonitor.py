import sys
import getopt
import tempfile
from BaseHTTPServer import HTTPServer
from pingThread import PingThread
from httpHandler import GetHandler
import shutil

port = 8888
ip = "0.0.0.0"
period = 1
max_ = 1000

class http_server:
    def __init__(self, thread, tmpdir):
        def handler(*args):
            GetHandler(thread, tmpdir, *args)
        self.server = HTTPServer((ip, port), handler)
        # server.serve_forever()

if __name__ == "__main__":
    try:
        optlist, args = getopt.getopt(sys.argv[1:], 'i:m:p:')
    except getopt.GetoptError as err:
        print str(err)
        sys.exit(2)

    for o, a in optlist:
        if o == "-i":
            period = int(a)
        elif o == "-m":
            max_ = int(a)
        elif o == "-p":
            port = int(a)
        else:
            assert False, "unhandled option"

    tmpdir = tempfile.mkdtemp(prefix="pingMonitor")
    print "Logdir: " + tmpdir
    thread = PingThread(args, period, tmpdir, max_)
    thread.start()
    http = http_server(thread, tmpdir)

    print "Serving on port " + str(port)
    try:
        http.server.serve_forever()
    except KeyboardInterrupt as intr:
        print "HTTP server stopped ..."
    finally:
        print "Stopping ping Thread ..."
        thread.join()
        print "Cleaning ..."
        shutil.rmtree(tmpdir)
        print "Goodbye!"
