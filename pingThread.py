import time
import socket
import os
from threading import Thread, Event
import ping


class PingThread(Thread):
    def __init__(self, hosts, period, tmpdir, max_):
        super(PingThread, self).__init__()
        self.hosts = hosts
        self.period = period
        self.stopRequest = Event()
        self.tmpdir = tmpdir
        self.max = max_

    def join(self, timeout=None):
        self.stopRequest.set()
        super(PingThread, self).join(timeout)

    def filename(self, host):
        return self.tmpdir + "/" + host + ".log"

    @staticmethod
    def resetfile(fname):
        with open(fname, "w"):
            pass

    def clean(self):
        files = os.listdir(self.tmpdir)
        for filename in files:
            if filename.endswith(".log"):
                os.remove(self.tmpdir + "/" + filename)

    def run(self):
        self.clean()
        for f in self.hosts:
            self.resetfile(self.filename(f))

        # start infinite loop
        n = 0
        while not self.stopRequest.is_set():
            # print self.hosts
            for host in self.hosts:
                try:
                    delay = ping.do_one(host, 1, 100)
                    out = "{0},{1}\n".format(time.time(), delay * 1000 if delay is not None else 0)
                except socket.error, e:
                    out = "{0},{1}\n".format(time.time(), 0)

                if n < self.max:
                    with open(self.filename(host), "a") as hostfile:
                        hostfile.write(out)
                else:
                    with open(self.filename(host), 'r') as fin:
                        data = fin.read().splitlines(True)
                    with open(self.filename(host), 'w') as fout:
                        data.append(out)
                        fout.writelines(data[1:])
            n = n + 1
            self.stopRequest.wait(self.period)
