Simple ping monitor and statistics tool
=======================================

Installation
------------
- pip install ping
- pip install http

Usage
-----
sudo python pingMonitor.py [-i 30] [-p 8888] [-m 1000] [host1] [host2] ... [hostN]

-i  ping interval

-p  web server port

-m  samples limit
