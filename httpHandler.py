from BaseHTTPServer import BaseHTTPRequestHandler
import json
import os

documentRoot = "./static/"


class GetHandler(BaseHTTPRequestHandler):
    def __init__(self, thread, tmpdir, *args):
        self.thread = thread
        self.tmpdir = tmpdir
        BaseHTTPRequestHandler.__init__(self, *args)

    def filename(self, host):
        return self.tmpdir + "/" + host + ".log"

    def serve_file(self, filename):
        f = open(filename, "rb")
        self.send_response(200)
        mime = "text/html"
        if (filename.endswith(".css")):
            mime = "text/css"
        elif (filename.endswith(".js")):
            mime = "application/javascript"
        elif (filename.endswith(".png")):
            mime = "image/png"

        self.send_header('Content-type', mime)
        self.end_headers()
        self.wfile.write(f.read())
        f.close()
        return

    def serve_logs(self):
        files = os.listdir(self.tmpdir)
        ret = {}
        for filename in files:
            host = filename[:-4]
            if filename.endswith(".log"):
                ret[host] = []
                f = open(self.tmpdir + "/" + filename, "rb")
                for line in f:
                    ret[host].append(map(lambda x: float(x), line.strip().split(",")))
                f.close()
        self.send_response(200)
        self.send_header('Content-type', 'application/json')
        self.end_headers()
        self.wfile.write(json.dumps(ret))

    def do_GET(self):
        if self.path == "/":
            self.serve_file(documentRoot + "index.html")
        elif self.path.endswith(".html") or \
                self.path.endswith(".js") or \
                self.path.endswith(".css") or \
                self.path.endswith(".png"):
            self.serve_file(documentRoot + self.path)
        elif self.path == "/logs":
            self.serve_logs()
        else:
            self.send_error(404, 'File Not Found: %s' % self.path)

        return

    def do_POST(self):
        if self.path == "/add":
            content_len = int(self.headers.getheader('content-length', 0))
            post_body = self.rfile.read(content_len)
            try:
                data = json.loads(post_body)
                self.thread.hosts.append(data["host"])
                print "Adding " + data["host"] + " to the host list"
                self.send_response(200)
                self.wfile.write("added")
            except ValueError as err:
                self.send_response(200)
                self.wfile.write("Error parsing JSON")
        elif self.path == "/remove":
            content_len = int(self.headers.getheader('content-length', 0))
            post_body = self.rfile.read(content_len)
            try:
                data = json.loads(post_body)
                os.remove(self.filename(data["host"]))
                self.thread.hosts.remove(data["host"])
                print "Removing " + data["host"] + " to the host list"
                self.send_response(200)
                self.wfile.write("removed")
            except ValueError as err:
                self.send_response(200)
                self.wfile.write("Error parsing JSON")

        else:
            self.send_error(404, 'File Not Found: %s' % self.path)
